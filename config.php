<?php

    $config_data =array();

#Opencart website URL(For getting image URLs)
    $config_data['opencart_website_url'] = '';# https://www.example.com

    $config_data['total_number_of_products_in_opencart_website']='';

#API key
#Go to Commerce.js dashboard->Developer->API keys and CORS

#-----Note from Commerce.js--------
#---------------------------------------------------------
#Cross-Origin Resource Sharing (CORS) origins

#You may configure a list of trusted domains (including the scheme and port)
# for CORS headers when using public API keys. If you configure one domain,
# all API responses will use that domain in the "Access-Control-Allow-Origin" header.
# If you specify multiple, API calls will compare against the request's "Origin" header.
# If you leave this blank, all origins will be allowed (default).
#---------------------------------------------------------

#Check your dashboard carefully before you set the API Key here.

#SET $API_Key here.
    $config_data['API_Key'] = '';

#$Merchant_ID
#---------------------------------------------------------
#You can find the merchant ID near your profile name in Commerce.js dashboard
    $config_data['Merchant_ID'] = '';

#MySQL settings
#---------------------------------------------------------

    $config_data['servername'] = "";
    $config_data['username']   = "";
    $config_data['password']   = "";
    $config_data['dbname']     = "";

?>