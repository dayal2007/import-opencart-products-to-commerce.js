<?php
class products_and_options
{
    public $db_connection;
    function dbconnection($config_data)
    {
        // Create connection
        $conn = new mysqli($config_data['servername'], $config_data['username'], $config_data['password'], $config_data['dbname']);

        // Check connection
        if ($conn->connect_error)
        {
            die("Connection failed: " . $conn->connect_error);
        }
        $this->db_connection = $conn;
        return $conn;
    }

    #Step: 1
    function add_options_to_products($config_data){

        if(!$this->db_connection){
            die("DB Connection failed");
        }

        $conn = $this->db_connection;

        //Fetching all products from cjs database
        $get_all_cjs_products = $this->get_all_cjs_products($config_data);



        ###################################################################
        //PRODUCT LOOP


        foreach($get_all_cjs_products[0] as $cjs_oc_product)
        {
            $opencart_product_id = $cjs_oc_product["cjs_opencart_id"];
            $cjs_product_id = $cjs_oc_product["cjs_product_id"];

            #-----------------------------------------------------------

            $sqlpo = "SELECT * FROM oc_product_option ocpo 
                      LEFT JOIN oc_product_option_value ocpov ON(ocpov.product_option_id = ocpo.product_option_id) 
                      LEFT JOIN oc_option oco ON(oco.option_id = ocpo.option_id) 
                      LEFT JOIN oc_option_value ocov ON(ocov.option_value_id = ocpo.option_value-_id) 
                      WHERE ocpov.product_id='".$opencart_product_id."'";
            $resultpo = $conn->query($sqlpo);

            #------------------------------------------------------------

            $sqlpo   = "SELECT *,ocod.name as oc_od_name FROM oc_product_option_value ocpov ";
            $sqlpo  .= "LEFT JOIN oc_option oco ON(oco.option_id=ocpov.option_id) ";
            $sqlpo  .= "LEFT JOIN oc_option_description ocod ON(ocod.option_id=ocpov.option_id) ";//for type eg: select box, radio button, check box etc.
            $sqlpo  .= "LEFT JOIN oc_option_value ocov ON(ocov.option_value_id=ocpov.option_value_id) ";// for option value image
            $sqlpo  .= "LEFT JOIN oc_option_value_description ocovd ON(ocovd.option_value_id=ocpov.option_value_id) ";// for option name(for variant group)

            $sqlpo  .= "WHERE ocpov.product_id='".$opencart_product_id."'";

            $resultpo = $conn->query($sqlpo);

            //GETTING PRODUCT OPTIONS(Variant group)
            $variant_group_ids = array();
            $variant_group_options = array();
            $variant_group_name = array();

            if ($resultpo->num_rows > 0) {
                while($row_po = $resultpo->fetch_assoc()) {

                    $variant_group_ids[] = $row_po['option_id'];
                    $variant_group_name[$row_po['option_id']] = $row_po['name'];

                    if($row_po['option_id']>0){
                        $variant_group_options[$row_po['option_id']][] = array(
                            'option_id'=>$row_po['option_id'],
                            'option_value_id'=>$row_po['option_value_id'],
                            'option_value_image'=>$row_po['image'],
                            'option_type'=>$row_po['type'],
                            'option_value_name'=>$row_po['oc_od_name'],//option_name for cjs
                            'option_name'=>$row_po['name'],//group_name for cjs
                            'option_quantity'=>$row_po['quantity'],
                            'option_price'=>$row_po['price'],
                            'option_price_prefix'=>$row_po['price_prefix'],
                            'product_option_value_id'=>$row_po['product_option_value_id'],
                        );
                    }

                }
            }
            if($variant_group_ids)
            {
                $variant_group_ids = array_unique($variant_group_ids);
            }



            foreach($variant_group_ids as $variant_group_id)
            {

                //create cjs_variant_group
                $this->add_options($variant_group_name[$variant_group_id],$variant_group_options[$variant_group_id],$cjs_product_id, $config_data);

            }




        }
        exit();
        //PRODUCT LOOP ENDS
        ###################################################################


        #----------------------------------------------------------------------------------

    }

    #Step: 2
    function add_options($variant_group_name, $variant_group_options, $cjs_product_id, $config_data)
    {

        $options_ids_string_object = '{"options" : ""}'; //derek changed
        $options_ids_string_object = json_decode($options_ids_string_object); //derek change to php object

        $options_ids_string_object->options = array(); //derek
        foreach($variant_group_options as $key => $option){ // derek added loop
            $string_replace = str_replace('&quot;or', 'inch or', $option['option_name']);
            $string_replace = str_replace('&quot;', 'inch', $string_replace);
            $options_ids_string_object_name = '{"name" : "'. $string_replace.'", "price" : " '. $option['option_price'] .' "}';
            $options_ids_string_object_name = json_decode($options_ids_string_object_name);
            $options_ids_string_object->options[$key] = $options_ids_string_object_name;
        }
        $encoded_options = json_encode($options_ids_string_object->options);

        $variant_group_name = str_replace('&quot;or', 'inch or', $variant_group_name);
        $variant_group_name = str_replace('&quot;', 'inch', $variant_group_name);

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_URL => $config_data['api_url_for_create_product_variants'].$cjs_product_id.'/variant_groups',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS =>'{
                                        "name": "'.$variant_group_name.'",
                                        "options": '.$encoded_options.'
                                        }',
            CURLOPT_HTTPHEADER => array(
                'X-Authorization:'.$config_data['API_Key'],
                'Content-Type: application/json'
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        $response = json_decode($response);

    }
    function get_all_cjs_products($config_data)
    {
        $totalproducts=$config_data['total_number_of_products_in_opencart_website'];

        $cjs_to_opencart_product_id = array();

        $number_of_pages = ceil($totalproducts/200);

        for($i=1 ; $i<=$number_of_pages ; $i++){

            $cjs_to_opencart_product_id[] = $this->pull_cjs_products($i,$config_data);

        }

        return $cjs_to_opencart_product_id;

    }

    function pull_cjs_products($page=1,$config_data)
    {
        //Get the opencart and cjs product_id
        $cjs_products = array();
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_URL => $config_data['api_url_for_update_product'],
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_POSTFIELDS =>'{
                                    "limit": 200,
                                    "page":1
                                  }',
            CURLOPT_HTTPHEADER => array(
                'X-Authorization:'.$config_data['API_Key'],
                'Content-Type: application/json'
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        $response = json_decode($response);

        foreach($response->data as $item)
        {
            $opencart_product_id = $item->meta->opencart_id;
            if($opencart_product_id)
            {

                $cjs_products[]=array("cjs_product_id"=>$item->id , "cjs_opencart_id"=>$opencart_product_id);

            }
        }

        return $cjs_products;
    }
}
?>