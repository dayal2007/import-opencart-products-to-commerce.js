<?php
# API version
# 2022-07-21
# LIST OF API URLS using in this lib.

//Get all products
//CURLOPT_CUSTOMREQUEST:GET
$config_data['api_url_for_get_all_products'] = 'https://api.chec.io/v1/products/?merchant='.$config_data['Merchant_ID'];

//Add new products
//CURLOPT_CUSTOMREQUEST:POST
$config_data['api_url_for_add_new_products'] = 'https://api.chec.io/v1/products?merchant='.$config_data['Merchant_ID'];

//Update product
//CURLOPT_CUSTOMREQUEST:PUT

$config_data['api_url_for_update_product'] = 'https://api.chec.io/v1/products/';


//Create assets(image files)
//CURLOPT_CUSTOMREQUEST:POST
$config_data['api_url_for_creating_assets'] = 'https://api.chec.io/v1/assets/?merchant='.$config_data['Merchant_ID'];


//Assign image files to products(image files)
//CURLOPT_CUSTOMREQUEST:POST
$config_data['api_url_for_assign_images_to_products']= 'https://api.chec.io/v1/products/';
#example: 'https://api.chec.io/v1/products/'.$product_id.'/assets';


//product variants
//CURLOPT_CUSTOMREQUEST:POST
$config_data['api_url_for_create_product_variants']= 'https://api.chec.io/v1/products/';
#example: 'https://api.chec.io/v1/products/'.$cjs_product_id.'/variant_groups',


//Get all product_categories
//CURLOPT_CUSTOMREQUEST:GET
$config_data['api_url_for_get_all_categories'] = 'https://api.chec.io/v1/categories';

//Get category data
//CURLOPT_CUSTOMREQUEST:GET

$config_data['api_url_for_get_category_data'] = 'https://api.chec.io/v1/categories/';


//Add new categorys
//CURLOPT_CUSTOMREQUEST:POST
$config_data['api_url_for_add_new_category'] = 'https://api.chec.io/v1/categories';

//Update category
//CURLOPT_CUSTOMREQUEST:PUT
    if(isset($cjs_category_id))
    {
        $config_data['api_url_for_update_category'] = 'https://api.chec.io/v1/categories/'.$cjs_category_id;
    }


?>