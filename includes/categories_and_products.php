<?php
class categories_and_products
{
    public $db_connection;
    public function dbconnection($config_data)
    {
        // Create connection
        $conn = new mysqli($config_data['servername'], $config_data['username'], $config_data['password'], $config_data['dbname']);

        // Check connection
        if ($conn->connect_error)
        {
            die("Connection failed: " . $conn->connect_error);
        }
        $this->db_connection = $conn;
        return $conn;
    }
    #Step : 1
    function create_category_database($config_data){

        if(!$this->db_connection){
            die("DB Connection failed");
        }

        $conn = $this->dbconnection($config_data);
        $sql = "SELECT * FROM oc_category occ LEFT JOIN oc_category_description occd ON(occd.category_id=occ.category_id) WHERE occ.parent_id=0";
        $result = $conn->query($sql);
        if ($result->num_rows > 0) {
            // output data of each row
            while($row = $result->fetch_assoc()) {
                //echo $row['category_id'].'<br>';

                $data = array(
                    "name" => $row["name"],
                    "description"=> $row["description"],
                    "slug"=> $row["category_id"],
                    "config" => $config_data
                );
                $insert_data = $this->add_category($data);

                $insert_data = json_decode($insert_data);
                $insert_id = $insert_data->id;
                //var_dump($insert_data);
                //Checking child category

                $sql_child_data = "SELECT * FROM oc_category occ LEFT JOIN oc_category_description occd ON(occd.category_id=occ.category_id)  WHERE occ.parent_id = '".$row['category_id']."'";
                $result_child_data = $conn->query($sql_child_data);

                if ($result_child_data->num_rows > 0) {
                    while($row_child_data = $result_child_data->fetch_assoc()) {
                        $data_child = array(
                            "name" => $row_child_data["name"],
                            "description"=>$row_child_data["description"],
                            "slug"=>$row_child_data["category_id"],
                            "parent_id"=>$insert_id
                        );
                        $this->add_category($data_child);

                    }
                }

                //print_r($data['slug']);
            }echo 'Success all categories created';
        } else {
            echo "0 categories found!";
        }

        $conn->close();

    }

    #Step : 2
    function add_category($data)
    {
        $parent_id = '';
        $curl = curl_init();
        $name = $data["name"];
        $description = $data["description"];
        $slug = trim($data["name"]);
        $slug = str_replace(' ', '-', $slug);
        $slug = strtolower($slug);

        $config_data = $data['config'];


        if(isset($data["parent_id"])){
            $slug = strtolower($slug).'-'.$data["parent_id"];
            $parent_id = $data["parent_id"];
        }

        curl_setopt_array($curl, array(
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_URL => $config_data['api_url_for_add_new_category'],
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS =>'{
                                     
                                        "name": "'.$name.'",
                                        "description": "'.$description.'",
                                        "slug" : "'.$slug.'",
                                        "parent_id" : "'.$parent_id.'",
                                        "meta" : {
                                                    "opencart_category_id" : "'.$data["slug"].'"  
                                                  }
                                     
                                   }',
            CURLOPT_HTTPHEADER => array(
                'X-Authorization: '.$config_data['API_Key'],
                'Content-Type: application/json'
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        return $response;
    }

    #Step : 3
    function create_product_database($config_data){

        if(!$this->db_connection){
            die("DB Connection failed");
        }

        $conn = $this->db_connection;
        $sql = "SELECT * FROM oc_product ocp LEFT JOIN oc_product_description ocpd ON(ocpd.product_id=ocp.product_id)";
        $result = $conn->query($sql);

        $all_categories_in_commercejs = $this->get_all_categories_in_commercejs($config_data);

        $data['all_categories_in_commercejs']=$all_categories_in_commercejs;

        if ($result->num_rows > 0) {
            // output data of each row
            while($row = $result->fetch_assoc()) {
                //echo $row['product_id'].'<br>';

                $data_for_product = array(
                    "name" => $row["name"],
                    "description" => $row["description"],
                    "price"=>$row["price"],
                    "product_id"=>$row["product_id"],
                    "quantity"=>$row["quantity"],
                    "image" => $row["image"],
                    "all_categories_in_commercejs"=>$data['all_categories_in_commercejs'],
                    "config"=>$config_data
                );
                $this->addproduct($data_for_product);
            }
        } else {
            echo "0 results";
        }
        $conn->close();

    }

    #Step : 4
    function addproduct($data)
    {
        $config_data = $data['config'];
        if(!$this->db_connection){
            die("DB Connection failed");
        }


        $category_ids_string ='';
        $conn = $this->db_connection;


        //Getting all categories from commercejs
        $all_categories_in_commercejs = $data['all_categories_in_commercejs'];

        //Checking the categories assigned to this product
        $sql = "SELECT * FROM oc_product_to_category WHERE product_id = '".$data['product_id']."' Limit 3";
        $result = $conn->query($sql);


        $category_ids = array();
        if ($result->num_rows > 0) {
            // output data of each row



            while($row = $result->fetch_assoc())
            {


                foreach($all_categories_in_commercejs as $category)
                {
                    if($category['opencart_id']==$row['category_id'])
                    {
                        $category_ids[]=$category['cjs_category_id'];
                    }
                }


            }



        } else {
            echo "0 results";
        }

        //$conn->close();

        $category_ids_string_object = '{"categories" : ""}'; //derek changed
        $category_ids_string_object = json_decode($category_ids_string_object); //derek change to php object


        $category_ids_string = '{"id" : "';
        $category_ids = array_unique($category_ids);
        $category_ids_string_object->categories = array(); //derek
        foreach($category_ids as $key => $id){ // derek added loop
            $category_ids_string_object_id = '{"id" : "'.$id.'"}';
            $category_ids_string_object_id = json_decode($category_ids_string_object_id);
            $category_ids_string_object->categories[$key] = $category_ids_string_object_id;
        }


        $param_categories = json_encode($category_ids_string_object->categories);
        $category_ids_string .= implode('", "id": "',$category_ids);
        $category_ids_string .= '"}';

        if($category_ids_string === '{"id" : ""}'){
            $post_options = '{
                                        "product":{
                                            "name": "'.$data['name'].'",
                                            "description": "'.$data['description'].'",
                                            "meta": {"opencart_id":"'.$data['product_id'].'"},
                                            "name": "'.$data['name'].'",
                                            "price": "'.$data['price'].'",
                                            "inventory": {
                                                "managed": true,
                                                "available": "'.$data['quantity'].'"
                                                }
                                            
                                        },
                                        "delivery": {
                                                "enabled": {
                                                    "shipping_native_v1" : true
                                                }, 
                                                "shipping_zones": [
                                                {
                                                    "zone_id": "zone_bO6J5aPeyoEjpK",
                                                    "rates": [
                                                        {"base_rate_id": "rate_DWy4oGYWYl6Jx2"}
                                                    ]
                                                }
                                                ]
                                            },
                                        }';
        }else{
            $post_options = '{
                                "product":{
                                    "name": "'.$data['name'].'",
                                    "description": "'.$data['description'].'",
                                    "meta": {"opencart_id":"'.$data['product_id'].'"},
                                    "price": "'.$data['price'].'",
                                    "inventory": {
                                                "managed": true,
                                                "available": "'.$data['quantity'].'"
                                                }
                                },
                                "delivery": {
                                                "enabled": {
                                                    "shipping_native_v1" : true
                                                }, 
                                                "shipping_zones": [
                                                {
                                                    "zone_id": "zone_bO6J5aPeyoEjpK",
                                                    "rates": [
                                                        {"base_rate_id": "rate_DWy4oGYWYl6Jx2"}
                                                    ]
                                                }
                                                ]
                                            },
                                "categories": '.$param_categories.
                '}';
        }

        //Getting assigned categories for this product from opencart db
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_URL => $config_data['api_url_for_add_new_products'],
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => $post_options,
            CURLOPT_HTTPHEADER => array(
                'X-Authorization: '.$config_data['API_Key'],
                'Content-Type: application/json'
            ),
        ));

        $response = curl_exec($curl);

        $response = json_decode($response);

        /*
         * Add main image to created product
         * */
        if(isset($data['image']) && $data['image'] !== ""){
            $main_image = $this->createAsset($data['image'],$config_data);
            $this->assignImageToProduct($response->id, $main_image->id, $config_data);
        }

        /*
         * Add all the other assets
         * */
        $this->createAllAssets($data['product_id'], $response->id, $config_data);

        curl_close($curl);
        return $response;
    }

    #Step : 5
    function get_all_categories_in_commercejs($config_data)
    {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_URL => $config_data['api_url_for_get_all_categories'],
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_POSTFIELDS =>'{ 
                                    "params":{
                                                "depth" : "50"
                                             }
                                  }',
            CURLOPT_HTTPHEADER => array(
                'X-Authorization: '.$config_data['API_Key'],
                'Content-Type: application/json'
            ),
        ));

        $response = curl_exec($curl);

        $response = json_decode($response);
        curl_close($curl);

        $category_array =array();



        foreach($response->data as $child)
        {
            //print '<pre>';
            //print_r($child->children);
            //print '</pre>';
            if(isset($child->id) && isset($child->meta->opencart_category_id)){
                $category_array[] = array('cjs_category_id'=> $child->id , 'opencart_id'=> $child->meta->opencart_category_id );

                foreach($child->children as $children)
                {
                    $comercejs_category_id = $children->id;
                    $child_category_data = $this->get_category($comercejs_category_id, $config_data);
                    $category_array[] = array('cjs_category_id'=> $child_category_data->id, 'opencart_id'=> $child_category_data->meta->opencart_category_id);
                }
            }
        }
        return $category_array;
    }

    #Step : 6
    function createAsset($image,$config_data){

        $file_name = strstr($image, '/');
        $file_name = ltrim($file_name, $file_name[0]);
        $image = urldecode($image);

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_URL => $config_data['api_url_for_creating_assets'],
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS =>'{
                                       "filename": "'.$file_name.'",
                                       "url": "'.$config_data['opencart_website_url'].'/image/'.$image.'"
                                   }',
            CURLOPT_HTTPHEADER => array(
                'X-Authorization: '.$config_data['API_Key'],
                'Content-Type: application/json'
            ),
        ));

        $response = curl_exec($curl);
        $response = json_decode($response);


        curl_close($curl);
        return $response;
    }
    #Step: 7
    function get_category($comercejs_category_id, $config_data)
    {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_URL => $config_data['api_url_for_get_category_data'].$comercejs_category_id,

            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_POSTFIELDS =>'{ 
                                    "params":{
                                                "depth" : "2"
                                             }
                                  }',
            CURLOPT_HTTPHEADER => array(
                'X-Authorization: '.$config_data['API_Key'],
                'Content-Type: application/json'
            ),
        ));

        $response = curl_exec($curl);
        curl_close($curl);

        return $response = json_decode($response);

    }

    #Step: 8
    function assignImageToProduct($product_id, $image_id, $config_data){
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_URL => $config_data['api_url_for_assign_images_to_products'].$product_id.'/assets',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS =>'{
                                    "assets": [
                                            {"id": "'.$image_id.'"}
                                        ]
                                }',
            CURLOPT_HTTPHEADER => array(
                'X-Authorization: '.$config_data['API_Key'],
                'Content-Type: application/json'
            ),
        ));
        $response = curl_exec($curl);

        $response = json_decode($response);

        curl_close($curl);
        return $response;
    }


    #Step: 9
    function createAllAssets($product_id, $commerce_product_id, $config_data){

        if(!$this->db_connection){
            die("DB Connection failed");
        }

        $conn = $this->db_connection;
        $sql = "SELECT * FROM oc_product_image where product_id=".$product_id;
        $result = $conn->query($sql);
        if ($result->num_rows > 0) {
            // output data of each row
            while($row = $result->fetch_assoc()) {
                $data_for_product_image = array(
                    "product_image_id" => $row["product_image_id"],
                    "product_id"=>$row["product_id"],
                    "image" => $row["image"],
                );
                var_dump($data_for_product_image);
                if(isset($data_for_product_image['product_id'])){
                    $asset_id = $this->createAsset($data_for_product_image['image'],$config_data);
                    $this->assignImageToProduct($commerce_product_id, $asset_id->id,$config_data);
                }
            }
        } else {
            echo "0 results";
        }
    }
}
?>