<?php
require_once ("config.php");
require_once ("includes/api_urls.php");
require_once ("includes/initialization.php");

require_once ("includes/categories_and_products.php");
require_once ("includes/products_and_options.php");

$do = '';
if(isset($_GET['do'])){
    $do = $_GET['do'];

    $Obj_categories_and_products = new categories_and_products();
    #Create database connection
    $Obj_categories_and_products->dbconnection($config_data);

    $Obj_products_and_options    = new products_and_options();
    #Create database connection
    $Obj_products_and_options->dbconnection($config_data);

    // CATEGORIES
    #Step 1: Create database connection
    if($do=='create_categories')
    {
        $Obj_categories_and_products->create_category_database($config_data);
    }

    // PRODUCTS
    #Step 1: Create products
    if($do=='create_products')
    {
        $Obj_categories_and_products->create_product_database($config_data);
    }

    if($do=='create_product_options')
    {
        $Obj_products_and_options->add_options_to_products($config_data);
    }
}


?>
<h1>Create Your commerce.js database from Opencart database</h1>

<code>Step 1: Copy the files from Git</code><br>
<code>Step 2: Configure the application by setting the variables in config.php</code><br>
<code>Step 3: Click the links shown below one by one as per the serial number</code><br>
<code>Step 4: Please wait for the completion of each step and click the next link to start the process</code><br>
<code>Step 5: Test the data for correctness and errors, if any errors found delete all data from commerce.js db and repeat the process</code><br>
<ul style="list-style: -moz-ethiopic-numeric">
    <li><a target="_blank" href="?do=create_categories">Create categories</a></li>
    <li><a target="_blank" href="?do=create_products">Create products and product images</a></li>
    <li><a target="_blank" href="?do=create_product_options">Create product options</a></li>
</ul>